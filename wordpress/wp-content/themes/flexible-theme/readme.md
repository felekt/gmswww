# Wordpress Boilerplate with Webpack, Browsersync and ACFPRO Flexible Theme
###### Wordpress Boilerplate with Webpack, Browsersync and ACFPRO Flexible Theme

## Recommended Plugins:
- Contact Form 7
- ACF PRO

**Installation:**
1. Install dependencies:
```
npm install -D
```
2. Run webpack
```
npm run serve
```
or 
```
npm run dist
```
for production build.