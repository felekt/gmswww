<?php
/**
 * Flexible Content Module: Test Content
 * 
 * example od use flexible content in template
 * 
 * example contain text field and wysiwyg
 * 
 * All elements are in variables $section, to show them, need use field's ID name 
 */



?>


<section class="section test-content">
    <div class="container">
        <div class="row">
            <div class="col-12 field__text">
                <?php echo $section['text']; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12 field__wysiwyg">
                <?php echo $section['wysiwyg']; ?>
            </div>
        </div>
    </div>
</section>